module gitlab.com/tanna.dev/missing-translations

go 1.19

require github.com/leonelquinteros/gotext v1.5.1

require golang.org/x/text v0.3.7 // indirect
