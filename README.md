# missing-translations

A command-line tool to determine whether gettext `.po` files are in-sync, or if any locales are missing keys.

## Usage

Install the binary:

```sh
go install gitlab.com/tanna.dev/missing-translations@main
```

### `.po` files

Then, noting that the quotes are required around the glob, to avoid the shell interpreting it:

```sh
missing-translations -po 'testdata/*.po'
```

If there are missing translations, you will receive an error status code, and a message such as for each key that is missing:

```
Key `success` does not have a translation in every locale, missing: [es.po]
```
