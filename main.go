package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/leonelquinteros/gotext"
)

func main() {
	var poGlob string
	flag.StringVar(&poGlob, "po", "", "A glob for .po files")
	flag.Parse()

	if poGlob == "" {
		flag.Usage()
		os.Exit(1)
	}

	matches, err := filepath.Glob(poGlob)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Glob provided, `%s`, was not valid: %v\n", poGlob, err)
		os.Exit(1)
	}
	if matches == nil {
		fmt.Fprintf(os.Stderr, "Glob provided, `%s`, did not produce any matches, check it was valid\n", poGlob)
		os.Exit(2)
	}

	var allLocales []string
	for _, v := range matches {
		allLocales = append(allLocales, filepath.Base(v))
	}

	translationKeys := make(map[string][]string)

	for _, v := range matches {
		po := gotext.NewPo()
		po.ParseFile(v)
		for k := range po.GetDomain().GetTranslations() {
			if _, ok := translationKeys[k]; !ok {
				translationKeys[k] = make([]string, 0)
			}
			translationKeys[k] = append(translationKeys[k], filepath.Base(v))
		}
	}

	allMissing := make(map[string][]string)

	allMatch := true
	for k, v := range translationKeys {
		if len(v) != len(allLocales) {
			allMatch = false
			missingForKey := missing(v, allLocales)
			for _, locale := range missingForKey {
				if _, ok := allMissing[locale]; !ok {
					allMissing[locale] = make([]string, 1)
				}
				allMissing[locale] = append(allMissing[locale], k)
			}

			fmt.Printf("Key `%s` does not have a translation in every locale, missing: %v\n", k, missingForKey)

		}
	}
	if !allMatch {
		for k, vs := range allMissing {
			fmt.Printf("Locale `%s` is missing %d keys:%s\n", k, len(vs), strings.Join(vs, "\n- "))
		}
		os.Exit(1)
	}
}
