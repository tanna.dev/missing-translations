package main

// https://stackoverflow.com/a/57448769
// empty struct (0 bytes)
type void struct{}

// missing compares two slices and returns slice of differences
func missing(a, b []string) []string {
	// create map with length of the 'a' slice
	ma := make(map[string]void, len(a))
	diffs := []string{}
	// Convert first slice to map with empty struct (0 bytes)
	for _, ka := range a {
		ma[ka] = void{}
	}
	// find missing values in a
	for _, kb := range b {
		if _, ok := ma[kb]; !ok {
			diffs = append(diffs, kb)
		}
	}
	return diffs
}

// end https://stackoverflow.com/a/57448769
